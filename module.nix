{ lib, pkgs, config, ... }:

with lib;
{
  options.services.webhook2muc = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable the bot";
    };
    jid = mkOption {
      type = types.str;
      description = "Jabber-ID";
    };
    password = mkOption {
      type = types.str;
      description = "Jabber password";
    };
    muc = mkOption {
      type = types.str;
      description = "MUC Jabber-ID";
    };
    httpPort = mkOption {
      type = types.int;
      default = 8080;
      description = "HTTP listening port";
    };
    headerChecks = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "HTTP header checks";
    };
  };

  config.systemd.services.webhook2muc =
    let
      cfg = config.services.webhook2muc;
      webhook2muc = (pkgs.callPackage ./default.nix {})
        .overrideAttrs (oldAttrs: {
          # Do not run `cargo test`
          doCheck = false;
        });
    in {
      description = "Webhook to MUC bot";
      wantedBy = [ "multi-user.target" ];
      after    = [ "network.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = ''
          ${webhook2muc}/bin/tokio-webhook2muc \
              -j "${cfg.jid}" \
              -p "${cfg.password}" \
              -m "${cfg.muc}" \
              -P "${toString cfg.httpPort}" \
         '' +
         (if builtins.length cfg.headerChecks == 0
          then "\n"
          else "    -H" + concatMapStrings (headerCheck: " \"${headerCheck}\"") cfg.headerChecks);

        Restart = "always";
        RestartSec = "5sec";

        DynamicUser = true;
        NoNewPrivileges = true;
        LimitNPROC = 1;
        LimitNOFILE = 32;
        CPUWeight = 5;
        MemoryMax = "128M";
        ProtectSystem = "full";
        CapabilityBoundingSet = "CAP_NET_BIND_SERVICE";
     };
  };
}
